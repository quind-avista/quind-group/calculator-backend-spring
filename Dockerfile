FROM java:8-jdk-alpine
FROM openjdk:8u111-jdk-alpine
COPY ./build/libs/calc-0.0.1-SNAPSHOT.jar /usr/app/
WORKDIR /usr/app
EXPOSE 9091
ENTRYPOINT ["java", "-jar", "calc-0.0.1-SNAPSHOT.jar"]