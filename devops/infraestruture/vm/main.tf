provider "azurerm" {
    # The "feature" block is required for AzureRM provider 2.x.
    # If you're using version 1.x, the "features" block is not allowed.

    subscription_id = "6fea5c6b-073f-4a0d-9a56-51e719c4b283"
    client_id       = "49973fb9-2f68-41b5-81ec-6a7dbe123b36"
    client_secret   = "ae38cb34-bc52-431a-a37d-c12f783924ff"
    tenant_id       = "f2567840-b2b7-49ad-8240-568a42281394"  

    features {}
}

variable "prefix" {
  default = "calculatorbackendspring"
}

variable "admin_ssh_publickey" {
  description = "Configure all the linux virtual machines in the cluster with the SSH RSA public key string. The key should include three parts, for example 'ssh-rsa AAAAB...snip...UcyupgH azureuser@linuxvm"
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQD7Q6zK/30Iq5dis1kxF4iO93TYNef9eu3ZZduVyHVocIu9DfZ4xOh/+NLcRpagiWPbAuijuSPyCs1qAAuTE8bZJq9xpaD5+3PWT/xiymhQ7ZDF1QRQtoZ4FJSNH39nVWSV3FZOXHxD3cF2WYDmVxLT6rIA4EQNVIq3A5JFBlS3foUuIF9+cYb1SsdiaitUQggaJoaU5lP4mygawSaZY6iCi/B1DKUdi8ld5NsdeLsWjTOB28Nyz+oW65m6gWcG0xrkU8DzUcoV7giVOvhbia9QLobdXEnJCKOwErE/0yjcHWyw9dVPt7Tc1w3RP9gsO6SLbwi0nlMMj0bSKUuT38lnATYws4sIUGvgIusu6/5VzKQE9teByBxp3yIB1Wq8VW1uKaZQH6vqQEgOIvT+pPoayhGysLvcTFwea04qWy/6awaZAzFFExzGfZdtAx7Bo1lrC8GHZlzYvNSQ6mn+udon9ZZydp/fLZerC6DZWz+dXNPz1bL8GHwsfLMnxcQwqgM= kevinestupinan@dev.azure.com"
}

resource "azurerm_virtual_network" "main" {
  name                = "${var.prefix}-network"
  address_space       = ["10.0.0.0/16"]
  location            = "West US"
  resource_group_name = "resource-group-kevin"
}

resource "azurerm_public_ip" "main" {
  name                    = "${var.prefix}-public_ip"
  location                = "West US"
  resource_group_name     = "resource-group-kevin"
  domain_name_label       = "quind-kevin-vm"
  allocation_method       = "Static"
}

resource "azurerm_subnet" "internal" {
  name                 = "internal"
  resource_group_name  = "resource-group-kevin"
  virtual_network_name = azurerm_virtual_network.main.name
  address_prefix       = "10.0.2.0/24"
}

resource "azurerm_network_interface" "main" {
  name                = "${var.prefix}-nic"
  location            = "West US"
  resource_group_name = "resource-group-kevin"

  ip_configuration {
    name                          = "testconfiguration1"
    subnet_id                     = azurerm_subnet.internal.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = azurerm_public_ip.main.id 
  }
}

resource "azurerm_virtual_machine" "main" {
  name                  = "${var.prefix}-vm"
  location              = "West US"
  resource_group_name   = "resource-group-kevin"
  network_interface_ids = [azurerm_network_interface.main.id]
  vm_size               = "Standard_B2s"

  # Uncomment this line to delete the OS disk automatically when deleting the VM
  delete_os_disk_on_termination = true

  # Uncomment this line to delete the data disks automatically when deleting the VM
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }
  storage_os_disk {
    name              = "myosdisk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name  = "hostname"
    admin_username = "testadmin"
    admin_password = "Password1234!"
  }
  os_profile_linux_config {
    disable_password_authentication = false

    ssh_keys {
      path = "/home/testadmin/.ssh/authorized_keys"
      key_data = "${var.admin_ssh_publickey}"
    }

  }
  tags = {
    environment = "Staging"
  }
}

# Get IP Address to use in SCP
output "public_ip" {
  value = "${azurerm_public_ip.main.ip_address}"
}