package io.quind.calc;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.Assert.*;

@SpringBootTest
class CalcApplicationTests {

	int a = 12;
	int b = 42;
	int result_sum;
	OpeSum operationSum = new OpeSum(a, b, result_sum);

	@Test
	void sumTesting() {
		int expect_sum = 54;
		result_sum = operationSum.sum(a, b);
		assertEquals(expect_sum,result_sum);
	}

	int c = 22;
	int d = 43;
	int result;
	OpeSub operationSub = new OpeSub(c, d, result);

	@Test
	void subTesting(){
		int expect_sub = -20;
		result = operationSub.sub(c, d);
		assertNotEquals(expect_sub,result);
	}

	OpeMul operationMul = new OpeMul(c,d, result);

	@Test
	void mulTesting(){
		int expect_mul = 946;
		result = operationMul.mul(c, d);
		assertEquals(expect_mul,result);
	}

	double e = 52;
	double f = 10;
	double result_div;
	OpeDiv operationDiv = new OpeDiv(e,f,result_div);

	@Test
	// Ask Julian why doesn't work assertEquals in double
	void divTesting(){
		double expect_div = 5.2;
		result_div = operationDiv.div(e,f);
		assertEquals(expect_div,result_div,1e-15);
		System.out.println("All tests passed");
	}
}
