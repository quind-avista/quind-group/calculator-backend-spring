package io.quind.calc;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
public class CalcService {

    @GetMapping("/sum/{a}/{b}")
    public int sum(@PathVariable int a, @PathVariable int b){
        OpeSum operationSum = new OpeSum(a, b, 0);
        return operationSum.sum(a, b);
    }

    @GetMapping("/sub/{a}/{b}")
    public int sub(@PathVariable int a, @PathVariable int b){
        OpeSub operationSub = new OpeSub(a, b, 0);
        return operationSub.sub(a, b);
    }

    @GetMapping("/mul/{a}/{b}")
    public int mul(@PathVariable int a, @PathVariable int b){
        OpeMul operationMul = new OpeMul(a, b, 0);
        return operationMul.mul(a, b);
    }

    @GetMapping("/div/{a}/{b}")
    public double div(@PathVariable double a, @PathVariable double b){
        OpeDiv operationDiv = new OpeDiv(a, b, 0);
        return operationDiv.div(a, b);
    }
}
